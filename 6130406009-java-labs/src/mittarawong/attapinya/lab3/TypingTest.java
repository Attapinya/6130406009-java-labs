package mittarawong.attapinya.lab3;

import java.util.*;

/**
 * This java program choose a random from rainbow and users have to type follow it for identify
 * if the users type faster or slower. 
 * 
 * @author Attapinya Mittarawong
 * Sec: 1
 * ID: 613040600-9
 * Date: 03/02/2019
 *
 */
public class TypingTest {
	public static void main(String[] args) {
		String arr = "";
		for (int i = 0;i < 7; i++ ) {
		Random rand = new Random();
		int rnd = rand.nextInt(7);
		
		int rainbow = rnd;
		
		String colors;
			switch (rainbow) {
				case 0: colors = "RED ";
						break;
				case 1: colors = "ORANGE ";
						break;
				case 2: colors = "YELLOW ";
						break;
				case 3: colors = "GREEN ";
						break;
				case 4: colors = "BLUE ";
						break;
				case 5: colors = "INDIGO ";
						break;
				case 6: colors = "VIOLET ";
						break;
				default: colors  = " ";
						break;
			}
		
			arr += colors;
		}
		
		System.out.println(arr);
		arr = arr.toLowerCase().trim();
		double startTime = System.currentTimeMillis();
		while (true) {
			System.out.print("Type your answer : ");
			Scanner input = new Scanner(System.in);
			
			String write = input.nextLine().toLowerCase().trim();
			
			if (arr.equals(write)) {
				
				double endTime = System.currentTimeMillis();
				double totalTime = (endTime - startTime) / 1000;
				System.out.println("Your time is " + totalTime);
				if (totalTime <= 12) {
					System.out.println("You type faster than average person");
					System.exit(0);
					input.close();
				}
				else {
					System.out.println("You type slower than average person");
					System.exit(0);
				}
			}
			else {
				continue;
			}
		}	
	}
}

