package mittarawong.attapinya.lab3;

import java.util.Random;
import java.util.Scanner;

/**
 * This java program use the same process as MathchingPenny but has more methods.
 * 
 * @author Attapinya Mittarawong
 * Sec: 1
 * ID: 613040600-9
 * Date: 03/02/2019
 *
 */
public class MatchingPennyMethod {
	
	static String acceptInput() {
		Scanner answer = new Scanner(System.in);
		System.out.print("Enter head or tail: ");
		String ans = answer.nextLine().toLowerCase();
		if (ans.equals("head") || ans.equals("tail")) {
			System.out.println("You play "+ ans);
		}
		else if (ans.equals("exit")) {
			System.out.println("Good bye");
			answer.close();
			System.exit(0);
		}
		else {
			System.err.println("Incorrect input. head or tail only.");
			acceptInput();
			}
		return ans;
	}
	
	static String genComChoice() {
		Random rand = new Random();
		int ran = rand.nextInt(2);
		String ansC;
		if (ran == 1) {
			System.out.println("Computer play head");
			ansC = "head";
		}
		else {
			System.out.println("Computer play tail");
			ansC = "tail";
		}
		return ansC;
	}
	/**
	 * 
	 * @param ans input argument from random number for computer using.
	 * @param ansC input argument from user.
	 */
	static void displayWiner(String ans, String ansC) {
		if (ansC.equals(ans)) {
			System.out.println("You Win.");
		}
		else {
			System.out.println("Computer Win.");
		}
	}
	/**
	 * 
	 * @param args input argument 
	 */
	public static void main(String[] args) {
		while (true) {
			String ans = acceptInput();
			String ansC = genComChoice();
			displayWiner(ans, ansC);
		}
	}
}
