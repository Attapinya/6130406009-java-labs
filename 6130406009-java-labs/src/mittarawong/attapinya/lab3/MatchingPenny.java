package mittarawong.attapinya.lab3;

import java.util.Random;
import java.util.Scanner;

/**
 * This java program that get input from users if the output is the same as computer using, we win
 * if it is not, computer win. And if it is error, users has to repeat the input again.
 * This java program will end by typing "exit".
 * 
 * @author Attapinya Mittarawong
 * Sec: 1
 * ID: 613040600-9
 * Date: 03/02/2019
 *
 */
public class MatchingPenny {

	public static void main(String[] args) {
		
		while (true) {
			Scanner answer = new Scanner(System.in);
			System.out.print("Enter head or tail: ");
			String ans = answer.nextLine().toLowerCase();
			
			if (ans.equals("head") || ans.equals("tail")) {
				System.out.println("You play "+ ans);
				Random rand = new Random();
				int ran = rand.nextInt(2);
				String ansC;
				if (ran == 1) {
					System.out.println("Computer play head");
					ansC = "head";
					if (ansC.equals(ans)) {
						System.out.println("You Win.");
					}
					else {
						System.out.println("Computer Win.");
					}
				}
				else {
					System.out.println("Computer play tail");
					ansC = "tail";
					if (ansC.equals(ans)) {
						System.out.println("You Win.");
					}
					else {
						System.out.println("Computer Win.");
					}
				}
			}
			else if (ans.equals("exit")) {
				System.out.println("Good bye");
				answer.close();
				System.exit(0);
			}
			else {
				System.err.println("Incorrect input. head or tail only.");
			}
			
		}
		
	}

}
