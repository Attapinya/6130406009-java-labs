package mittarawong.attapinya.lab3;

import java.util.Arrays;

/**
 * This java program BasicStat will find the minimum, the maximum,
 * the average and the standard deviation of the list of numbers entered.
 * The first argument is how many numbers to be entered. The rest of the program arguments are list of numbers.
 * This program will check if there are numbers entered as many as the first integer.
 * 
 * @author Attapinya Mittarawong
 * Sec: 1
 * ID: 613040600-9
 * Date: 03/02/2019
 *
 */
public class BasicStat {
	
	static double num[];
	static int numInput;
	/**
	 * 
	 * @param args input numbers
	 * This method acceptInput will check the number of entered numbers.
	 */
	
	public static void acceptInput(String[] args) {
		numInput = Integer.valueOf(args[0]);
		if (numInput == args.length - 1) {
			num = new double[numInput];
			for (int i = 0; i < numInput; i++) {
				double number = Double.valueOf(args[i+1]);
				num[i] = number;
			}
		}
		else {
			System.err.println("<BasicStat> <numNumbers> <numbers> ...");
			System.exit(0);
		}
	}
	/**
	 * This method is calculate min,max,average and standard deviation.
	 */
	public static void displayStats() {
		Arrays.sort(num);
		System.out.println("Min is " + num[0] + " Max is " + num[num.length - 1]);
		double average = 0;
		double sum = 0;
		for (int i = 0; i < num.length; i++) {
			sum += num[i];
		}
		average = sum / num.length;
		double sum1 = 0;
		System.out.println("Average is "+ average);
		double sd = 0;
		for(int i=0;i<num.length;i++){
			 sum1 += Math.pow(num[i]-average,2);
			 }
			 sd = Math.sqrt(sum1/num.length);
			 System.out.println("Standard Deviation is " + sd);
	}
	/**
	 * 
	 * @param args input argument.
	 */
	public static void main(String[] args) {
		acceptInput(args);
		displayStats();
	}
}
