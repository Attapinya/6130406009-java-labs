package mittarawong.attapinya.lab7;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

/**
 * This java program MobileDeviceFormV5 extends MobileDeviceFormV4
 * and set font and color of components.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-03-14
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MobileDeviceFormV5 extends MobileDeviceFormV4 {

	public MobileDeviceFormV5(String string) {
		super(string);
	}

	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	protected void addComponents() {
		super.addComponents();
		initComponents();
	}
	
	protected void addMenus() {
		super.addMenus();
	}
	
	protected void addSubMenu() {
		super.addSubMenu();
	}

	protected void updateMenuIcon() {
		super.updateMenuIcon();
	}
	
	/**
	 * initComponents set font and color of components.
	 */
	protected void initComponents() {
		labelBrand.setFont(new Font ("Serif", Font.PLAIN, 14));
		labelModel.setFont(new Font ("Serif", Font.PLAIN, 14));
		labelWeight.setFont(new Font ("Serif", Font.PLAIN, 14));
		labelPrice.setFont(new Font ("Serif", Font.PLAIN, 14));
		labelOS.setFont(new Font ("Serif", Font.PLAIN, 14));
		labelType.setFont(new Font ("Serif", Font.PLAIN, 14));
		labelFeatures.setFont(new Font ("Serif", Font.PLAIN, 14));
		labelReview.setFont(new Font ("Serif", Font.PLAIN, 14));
		
		brandInput.setFont(new Font ("Serif", Font.BOLD, 14));
		modelInput.setFont(new Font ("Serif", Font.BOLD, 14));
		weightInput.setFont(new Font ("Serif", Font.BOLD, 14));
		priceInput.setFont(new Font ("Serif", Font.BOLD, 14));
		
		textArea.setFont(new Font ("Serif", Font.BOLD, 14));
		
		cancelButton.setForeground(Color.RED);
		okButton.setForeground(Color.BLUE);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV5 mobileDeviceFormV5 = 
				new MobileDeviceFormV5("Mobile Device Form V5");
		mobileDeviceFormV5.addComponents();
		mobileDeviceFormV5.addMenus();
		mobileDeviceFormV5.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}

}
