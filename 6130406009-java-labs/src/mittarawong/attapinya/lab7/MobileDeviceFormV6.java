package mittarawong.attapinya.lab7;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * This java program MobileDeviceFormV6 extends MobileDeviceFormV5
 * and add image panel to the program.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-03-14
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MobileDeviceFormV6 extends MobileDeviceFormV5 {

	public MobileDeviceFormV6(String string) {
		super(string);
	}
	
	protected ImagePanel panel_image;
	protected JPanel panelCenter_V6;

	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	/**
	 * addComponents add the image panel to the program.
	 */
	protected void addComponents() {
		super.addComponents();
		panelAll.remove(panelSouth);
		
		panel_image = new ImagePanel("images/galaxyNote9.jpg");
		
		panelCenter_V6 = new JPanel();
		panelCenter_V6.setLayout(new BorderLayout());
		panelCenter_V6.add(panel_image, BorderLayout.NORTH);
		panelCenter_V6.add(panelSouth, BorderLayout.SOUTH);
		
		panelAll.add(panelCenter_V6, BorderLayout.SOUTH);	
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV6 mobileDeviceFormV6 = 
				new MobileDeviceFormV6("Mobile Device Form V6");
		mobileDeviceFormV6.addComponents();
		mobileDeviceFormV6.addMenus();
		mobileDeviceFormV6.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
}