package mittarawong.attapinya.lab7;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import mittarawong.attapinya.lab6.MobileDeviceFormV3;

/**
 * This java program MobileDeviceFormV4 extends MobileDeviceFormV3
 * and add icon for menu New and add sub menus for menu file and menu config.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-03-14
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MobileDeviceFormV4 extends MobileDeviceFormV3 {

	public MobileDeviceFormV4(String string) {
		super(string);
	}

	protected JMenuItem submenu_new, submenu_open, submenu_save, submenu_exit, subcolor_red, subcolor_green, subcolor_blue;
	protected JMenu submenu_color, submenu_size;
	
	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	protected void addComponents() {
		super.addComponents();
	}
	
	protected void addMenus() {
		super.addMenus();
		updateMenuIcon();
		addSubMenu();
	}
	
	/**
	 * addSubMenu remove menuConfig and add sub menus for color and size.
	 */
	protected void addSubMenu() {
		menuConfig.removeAll();
		
		submenu_color = new JMenu("Color");
		subcolor_red = new JMenuItem("Red");
		submenu_color.add(subcolor_red);
		subcolor_green = new JMenuItem("Green");
		submenu_color.add(subcolor_green);
		subcolor_blue = new JMenuItem("Blue");
		submenu_color.add(subcolor_blue);
		menuConfig.add(submenu_color);
		
		submenu_size = new JMenu("Size");
		submenu_size.add(new JMenuItem("16"));
		submenu_size.add(new JMenuItem("20"));
		submenu_size.add(new JMenuItem("24"));
		menuConfig.add(submenu_size);
	}

	/**
	 * updateMenuIcon remove menuFile and add icon for menu "New".
	 */
	protected void updateMenuIcon() {
		menuFile.removeAll();
		
		submenu_new = new JMenuItem("New", new ImageIcon("images/new.jpg"));
		menuFile.add(submenu_new);
		submenu_open = new JMenuItem("Open");
		menuFile.add(submenu_open);
		submenu_save = new JMenuItem("Save");
		menuFile.add(submenu_save);
		submenu_exit = new JMenuItem("Exit");
		menuFile.add(submenu_exit);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV4 mobileDeviceFormV4 = 
				new MobileDeviceFormV4("Mobile Device Form V4");
		mobileDeviceFormV4.addComponents();
		mobileDeviceFormV4.addMenus();
		mobileDeviceFormV4.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
}
