package mittarawong.attapinya.lab9;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import mittarawong.attapinya.lab8.MyBall;
import mittarawong.attapinya.lab8.MyBrick;
import mittarawong.attapinya.lab8.MyCanvas;

/**
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-10
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyCanvasV7 extends MyCanvasV6 implements Runnable {
	
	protected int numBricks = MyCanvas.WIDTH / MyBrick.brickWidth;
	protected MyBrickV2 brick[] = new MyBrickV2[numBricks];
	Thread running = new Thread(this);
	MyBallV2 ball = new MyBallV2(0, 0);
	
	public MyCanvasV7() {
		super();
		ball.ballVelX = 2;
		ball.ballVelY = 2;
		
		for (int i=0; i<numBricks; i++) {
			brick[i] = new MyBrickV2(MyBrick.brickWidth * i, MyCanvasV7.HEIGHT/2);
		}
		
		running.start();
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
		repaint();
		
		for (int i = 0; i < numBricks; i++) {
			g2d.setColor(Color.RED);
			g2d.fill(brick[i]);
			g2d.setColor(Color.BLUE);
			g2d.draw(brick[i]);

			if (brick[i].visible != true) {
				g2d.setColor(Color.WHITE);
				g2d.fill(ball);
				repaint();
				g2d.setColor(Color.BLACK);
				g2d.fill(brick[i]);
				g2d.draw(brick[i]);
			}
		}
	}
	
	@Override
	public void run() {
		
		while(true) {
			
			//bounce off the right and left of the wall
			if (ball.x <= 0 || ball.x + MyBall.diameter >= MyCanvas.WIDTH ) {
				ball.ballVelX *= -1;
			}
			
			// bouncing off the top and bottom
			if (ball.y <= 0 || ball.y + MyBall.diameter >= MyCanvas.HEIGHT ) {
				ball.ballVelY *= -1;
			}
			
			for (int i=0; i<numBricks; i++) {
				if (brick[i].visible) {
					checkCollision(ball, brick[i]);
				}
			}
			
			// move the ball
			ball.move();
			
			repaint();
			
			//Delay
			try
			{
				Thread.sleep(10);
			}
			catch(InterruptedException ex) 
			{
				
			}
		}
	}

	public void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;

		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) { //collides top or bottom
				// Change the direction on y coordinate to the opposite
				ball.ballVelY *= -1;
			
			} else { // collides left or right
				// Change the direction on x coordinate to the opposite
				ball.ballVelX *= -1;
			}
			ball.move();
			brick.visible = false;
		}
	}
}
