package mittarawong.attapinya.lab9;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import mittarawong.attapinya.lab8.MyBall;
import mittarawong.attapinya.lab8.MyBrick;
import mittarawong.attapinya.lab8.MyCanvas;
import mittarawong.attapinya.lab8.MyCanvasV4;
import mittarawong.attapinya.lab8.MyPedal;

/**
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-10
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyCanvasV5 extends MyCanvasV4 implements Runnable {

	protected MyBallV2 ball = new MyBallV2(0, MyCanvas.HEIGHT / 2 - MyBall.diameter / 2);
	protected Thread running = new Thread(this);
	
	public MyCanvasV5() {
		super();
		ball.ballVelX = 2;
		ball.ballVelY = 0;
		running.start();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		
		// Draw a ball
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
	}
	
	public void run() {
		while(true) {
			
			//Stop the ball when hit the right side of the wall
			if (ball.x + MyBall.diameter >= MyCanvas.WIDTH ) {
				break;
			}
			
			// move the ball
			ball.move();
			
			repaint();
			
			// Delay
			try
			{
				Thread.sleep(10);
			}
			catch(InterruptedException ex)
			{
				
			}
		}
	}
}
