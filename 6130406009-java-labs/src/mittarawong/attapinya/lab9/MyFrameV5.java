package mittarawong.attapinya.lab9;

import javax.swing.SwingUtilities;

import mittarawong.attapinya.lab8.MyCanvasV4;
import mittarawong.attapinya.lab8.MyFrameV4;

/**
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-10
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyFrameV5 extends MyFrameV4 {

	public MyFrameV5(String text) {
		super(text);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrameV5 msw = new MyFrameV5("My Frame V5");
		msw.addComponents();
		msw.setFrameFeatures();	
	}

	protected void addComponents() {
		add(new MyCanvasV5());
	}

}
