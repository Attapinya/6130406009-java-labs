package mittarawong.attapinya.lab9;

import mittarawong.attapinya.lab8.MyCanvas;
import mittarawong.attapinya.lab8.MyPedal;

/**
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-10
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyPedalV2 extends MyPedal {

	//declare speedPedal
	protected final static int speedPedal = 20;
	
	public MyPedalV2(int x, int y) {
		super(x, y);
	}
	
	//Add the code for moveLeft()
	public void moveLeft() {
		if (x - speedPedal < 0) {
			x = 0;
		} else {
			x += 50;
		}
	}
	
	//Add the code for moveRight()
	public void moveRight() {
		if (speedPedal + x > MyCanvas.WIDTH) {
			x = MyCanvas.WIDTH - MyPedal.pedalWidth;
		} else {
			x -= 50;
		}
	}

}
