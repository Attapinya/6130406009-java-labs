package mittarawong.attapinya.lab9;

import mittarawong.attapinya.lab8.MyBrick;

/**
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-10
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyBrickV2 extends MyBrick {
	
	protected boolean visible = true;

	public MyBrickV2(int x, int y) {
		super(x, y);
	}

}
