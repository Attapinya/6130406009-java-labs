package mittarawong.attapinya.lab9;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import mittarawong.attapinya.lab8.MyBall;
import mittarawong.attapinya.lab8.MyCanvas;

/**
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-10
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyCanvasV6 extends MyCanvasV5 implements Runnable {
	
	MyBallV2 ball = new MyBallV2(MyCanvas.WIDTH/2 - MyBall.diameter/2, MyCanvas.HEIGHT/2 - MyBall.diameter/2);
	Thread running = new Thread(this);

	public MyCanvasV6() {
		super();
		ball.ballVelX = 1;
		ball.ballVelY = 1;
		running.start();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
	}
	
	@Override
	public void run() {
		while(true) {
			
			//bounce the ball when hit the right or the left side of the wall
			if (ball.x <= 0 || ball.x + MyBall.diameter >= MyCanvas.WIDTH ) {
				ball.ballVelX *= -1;
			}
			
			// bouncing the ball when it hits the top and bottom of the wall
			if (ball.y <= 0 || ball.y + MyBall.diameter >= MyCanvas.HEIGHT ) {
				ball.ballVelY *= -1;
			}
			
			// move the ball
			ball.move();
			
			repaint();
			
			//Delay
			try {
				Thread.sleep(10);
			}
			catch(InterruptedException ex) { }
		}
	}
}
