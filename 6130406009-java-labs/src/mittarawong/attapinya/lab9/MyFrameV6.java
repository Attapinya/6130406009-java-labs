package mittarawong.attapinya.lab9;

import javax.swing.SwingUtilities;

/**
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-10
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyFrameV6 extends MyFrameV5 {

	public MyFrameV6(String text) {
		super(text);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrameV6 msw = new MyFrameV6("My Frame V6");
		msw.addComponents();
		msw.setFrameFeatures();	
	}

	protected void addComponents() {
		add(new MyCanvasV6());
	}

}
