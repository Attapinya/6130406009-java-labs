package mittarawong.attapinya.lab9;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;

import mittarawong.attapinya.lab8.MyBall;
import mittarawong.attapinya.lab8.MyBrick;
import mittarawong.attapinya.lab8.MyCanvas;
import mittarawong.attapinya.lab8.MyPedal;

/**
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-10
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyCanvasV8 extends MyCanvasV7 implements Runnable, KeyListener {

	protected int numCol = MyCanvas.WIDTH / MyBrick.brickWidth;
	protected int numRow = 7;
	protected int numVisibleBricks = numCol * numRow;
	protected MyBrickV2[][] bricks;
	protected Thread running;
	protected Color[] color = { Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE,
			Color.RED };
	protected MyPedalV2 pedal;
	protected int lives;
	
	public MyCanvasV8() {
		//initialize brick with numRow and numCol
		bricks = new MyBrickV2[numRow][numCol];
		
		// initialize ball with
		// MyCanvas.WIDTH/2 - MyBall.diameter/2 and
		// MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight
		ball = new MyBallV2(MyCanvas.WIDTH / 2 - MyBall.diameter / 2,
				MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight);
		ball.ballVelX = 0;
		ball.ballVelY = 0;
		
		running = new Thread(this);
		
		// initialize brick with j*MyBrick.brickWidth for x
		// and super.HEIGHT/3 - i*MyBrick.brickHeight for y
		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				bricks[i][j] = new MyBrickV2(j * MyBrick.brickWidth, super.HEIGHT / 3 - i * MyBrick.brickHeight);
			}
		}
		
		running.start();
		setFocusable(true);
		addKeyListener(this);
		
		// Initialize pedal with MyCanvas.WIDTH/2 - MyPedal.pedalWidth/2
		// and MyCanvas.HEIGHT - MyPedal.pedalHeight
		pedal = new MyPedalV2(MyCanvas.WIDTH / 2 - MyPedal.pedalWidth / 2, MyCanvas.HEIGHT - MyPedal.pedalHeight);
		
		lives = 3;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		
		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				if (bricks[i][j].visible) {
					g2d.setColor(color[i]); g2d.fill(bricks[i][j]);
					g2d.setStroke(new BasicStroke(4));
					g2d.setColor(Color.BLACK); g2d.draw(bricks[i][j]);
				}
			}
		}
		
		// Draw a ball
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
		
		// Draw a pedal
		g2d.setColor(Color.GRAY);
		g2d.fill(pedal);
		
		// Draw score
		g2d.setFont(new Font("SanSerif", Font.BOLD, 20));
		g2d.setColor(Color.BLUE);
		String s = "Lives : " + lives;
		g2d.drawString(s, 10, 30);
		
		// Draw a string You won
		if (numVisibleBricks == 0) {
			g2d.setColor(Color.GREEN);
			g2d.drawString("YOU WON", MyCanvas.WIDTH/2 - 150, MyCanvas.HEIGHT/2);
		}
		
		// Draw a string GAME OVER
		if (lives == 0) {
			g2d.setColor(Color.GRAY);
			g2d.drawString("GAME OVER", MyCanvas.WIDTH/2 - 150, MyCanvas.HEIGHT/2);
		}
	}

	public void checkPassBottom() {
		// TODO Auto-generated method stub
		if (ball.y >= MyCanvas.HEIGHT) {
			ball.x = pedal.x + MyPedal.pedalWidth / 2 - MyBall.diameter / 2;
			ball.y = MyCanvas.HEIGHT - MyBall.diameter - MyPedal.pedalHeight;
			ball.ballVelX = 0;
			ball.ballVelY = 0;
			lives--;
			repaint();
		}
	}
	
	public void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;

		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) { // collides top or bottom
				// Change the direction on y coordinate to the opposite
				ball.ballVelY *= -1;
			} else { // collides left or right
				// Change the direction on x coordinate to the opposite
				ball.ballVelX *= -1;
			}
			ball.move();
			brick.visible = false;
			numVisibleBricks--;
		}
	}

	public void collideWithPedal() {
		double x = ball.x + MyBall.diameter / 2.0;
		double y = ball.y + MyBall.diameter / 2.0;
		double deltaX = x - Math.max(pedal.x, Math.min(x, pedal.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(pedal.y, Math.min(y, pedal.y + MyBrick.brickHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.diameter * MyBall.diameter) / 4.0;

		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;
			} else {
				ball.ballVelX *= -1;
			}
			ball.move();
		}
	}
	
	public void keyPressed(KeyEvent arg0) {
		if (arg0.getKeyCode() == KeyEvent.VK_LEFT) { // check if press left arrow
			// move left
			pedal.moveLeft();
		} else if (arg0.getKeyCode() == KeyEvent.VK_RIGHT) { // check if press right arrow
			//move right
			pedal.moveRight();
		} else if (arg0.getKeyCode() == KeyEvent.VK_SPACE) { // check if press spacebar
			//set ballVelY to 4
			//then randomly set ballVelX to 4 or -4
			ball.ballVelY = 4;
			int[] rand = {4, -4};
			int random = (int) (Math.random() * ((1-0) + 1));
			ball.ballVelY = rand[random];
		}
	}

	@Override
	public void run() {
		while (true) {
			//bounce off the right and left of the wall.
			if (ball.x <= 0 || ball.x >= MyCanvas.WIDTH - MyBall.diameter) {
				ball.ballVelX *= -1;
			}

			//bounce off the top and bottom
			if (ball.y <= 0) {
				ball.ballVelY *= -1;
			}

			for (int i = 0; i < numRow; i++) {
				for (int j = 0; j < numCol; j++) {
					if (bricks[i][j].visible) {
						checkCollision(ball, bricks[i][j]);
					}
				}
			}
			
			//Check if ball collides with brick
			if (lives == 0) {
				break;
			} else if (numVisibleBricks == 0) {
				break;
			}
			
			//Check if ball hit pedal.
			collideWithPedal();
			
			//Check if ball pass the bottom
			checkPassBottom();

			// move the ball
			ball.move();
			
			repaint();
			
			// Delay
			try {
				Thread.sleep(20);
			} catch (InterruptedException ex) {

			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}
}
