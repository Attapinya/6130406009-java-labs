package mittarawong.attapinya.lab9;

import mittarawong.attapinya.lab8.MyBall;

/**
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-10
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyBallV2 extends MyBall {
	
	protected int ballVelX, ballVelY;

	public void move() {
		x += ballVelX;
		y += ballVelY;
	}
	
	public MyBallV2(int x, int y) {
		super(x, y);
	}
}
