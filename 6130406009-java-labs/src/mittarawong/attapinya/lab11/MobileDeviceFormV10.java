package mittarawong.attapinya.lab11;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import mittarawong.attapinya.lab10.MobileDeviceFormV9;
import mittarawong.attapinya.lab5.MobileDevice;

/**
 * 
 * @author Attapinya Mittarawong
 * @since 2019-05-09
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MobileDeviceFormV10 extends MobileDeviceFormV9 implements ActionListener {

	protected MobileDevice mobileDevice;
	
	protected ArrayList<MobileDevice> mobileDeviceList = new ArrayList<MobileDevice>();
	protected JMenu menuData;
	protected JMenuItem submenuDisplay, submenuSort, submenuSearch, submenuRemove;
	
	public MobileDeviceFormV10(String topic) {
		super(topic);
	}

	protected void handleOKButton() {
		super.handleOKButton();
		addMobileDevice();
	}
	
	private void addMobileDevice() {
		if (android.isSelected()) {
			mobileDeviceList.add(new MobileDevice(modelInput.getText(), "Android",
				Integer.parseInt(priceInput.getText()), Integer.parseInt(weightInput.getText())));
		} 
			
		else if (ios.isSelected()) {
			mobileDeviceList.add(new MobileDevice(modelInput.getText(), "ios", Integer.parseInt(priceInput.getText()),
				Integer.parseInt(weightInput.getText())));
		}

			System.out.println(mobileDeviceList);

	}
	
	protected void addMenus() {
		super.addMenus();
		
		menuData = new JMenu("Data");
		menubar.add(menuData);
		
		submenuDisplay = new JMenuItem("Display");
		menuData.add(submenuDisplay);
		
		submenuSort = new JMenuItem("Sort");
		menuData.add(submenuSort);
		
		submenuSearch = new JMenuItem("Search");
		menuData.add(submenuSearch);
		
		submenuRemove = new JMenuItem("Remove");
		menuData.add(submenuRemove);
	}

	protected void displaymobiledevice() {
		String display_text = "";
		for (int i = 0; i < mobileDeviceList.size(); i++) {
			display_text += Integer.toString(i + 1) + ": " + mobileDeviceList.get(i) + "\n";
		}
		JOptionPane.showMessageDialog(null, display_text);
	}
	
	protected void sortmobiledevice() {
		Collections.sort(mobileDeviceList, new PriceComparator());
		displaymobiledevice();
	}
	
	private void searchmobiledevice() {
		String searchName = JOptionPane.showInputDialog("Please input model name to search: ");
		
		for (int i = 0; i < mobileDeviceList.size(); i++) {
			if (mobileDeviceList.get(i).getModelName().equals(searchName)) {
				JOptionPane.showMessageDialog(null, mobileDeviceList.get(i) + " is found");
				break;
			} else if (i == mobileDeviceList.size() - 1) {
				JOptionPane.showMessageDialog(null, searchName + " is NOT found");
			}
		}
	}

	private void removemobiledevice() {
		String searchName = JOptionPane.showInputDialog("Please input model name to search: ");
		
		for (int i = 0; i < mobileDeviceList.size(); i++) {
			if (mobileDeviceList.get(i).getModelName().equals(searchName)) {
				JOptionPane.showMessageDialog(null, mobileDeviceList.get(i) + " is remove");
				mobileDeviceList.remove(i);
				break;
			} else if (i == mobileDeviceList.size() - 1) {
				JOptionPane.showMessageDialog(null, searchName + " is NOT found");
			}
		}
	}

	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		
		if (src == submenuDisplay) {
			displaymobiledevice();
		} else if (src == submenuSort) {
			sortmobiledevice();
		} else if (src == submenuSearch) {
			searchmobiledevice();
		} else if (src == submenuRemove) {
			removemobiledevice();
		}
	}
	
	protected void addListeners() {
		super.addListeners();
		submenuDisplay.addActionListener(this);
		submenuSort.addActionListener(this);
		submenuSearch.addActionListener(this);
		submenuRemove.addActionListener(this);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV10 mobileDeviceFormV10 = 
				new MobileDeviceFormV10("Mobile Device Form V10");
		mobileDeviceFormV10.addComponents();
		mobileDeviceFormV10.addMenus();
		mobileDeviceFormV10.setFrameFeatures();
		mobileDeviceFormV10.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}

}

class PriceComparator implements Comparator<MobileDevice> {
	public int compare(MobileDevice a, MobileDevice b) {
		return a.getPrice() < b.getPrice() ? -1 : a.getPrice() == b.getPrice() ? 0 : 1;
	}
}

