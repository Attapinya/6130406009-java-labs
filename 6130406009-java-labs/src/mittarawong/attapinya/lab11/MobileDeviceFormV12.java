package mittarawong.attapinya.lab11;

import java.awt.event.ActionEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;

import mittarawong.attapinya.lab5.MobileDevice;

/**
 * 
 * @author Attapinya Mittarawong
 * @since 2019-05-09
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MobileDeviceFormV12 extends MobileDeviceFormV11 {

	public MobileDeviceFormV12(String topic) {
		super(topic);
	}

	@SuppressWarnings("unchecked")
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		
		try {
			if (src == submenu_save) {
				if (returnval == JFileChooser.APPROVE_OPTION) {
					FileOutputStream fos = new FileOutputStream(menuFile.toString());
					ObjectOutputStream oos = new ObjectOutputStream(fos);
					oos.writeObject(mobileDeviceList);
					fos.close();
					oos.close();
				}
			} 
			else if (src == submenu_open) {
				if (returnval == JFileChooser.APPROVE_OPTION) {
					FileInputStream fis = new FileInputStream(menuFile.toString());
					ObjectInputStream ois = new ObjectInputStream(fis);
					mobileDeviceList = (ArrayList<MobileDevice>) ois.readObject();
					System.out.println(mobileDeviceList);
					displaymobiledevice();
					fis.close();
					ois.close();
				}
			}
		} catch (IOException ex) {

		} catch (Exception ex) {

		}
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV12 mobileDeviceFormV12 = 
				new MobileDeviceFormV12("Mobile Device Form V12");
		mobileDeviceFormV12.addComponents();
		mobileDeviceFormV12.addMenus();
		mobileDeviceFormV12.setFrameFeatures();
		mobileDeviceFormV12.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
}
