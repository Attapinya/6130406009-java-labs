package mittarawong.attapinya.lab11;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * 
 * @author Attapinya Mittarawong
 * @since 2019-05-09
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MobileDeviceFormV11 extends MobileDeviceFormV10 {

	protected int MIN_WEIGHT = 100 , MAX_WEIGHT = 3000 ;
	
	public MobileDeviceFormV11(String topic) {
		super(topic);
	}

	protected void handleOKButton() {
		try {
			if (modelInput.getText().equals("")) {
				JOptionPane.showMessageDialog(null, "Please enter model name");
			} else {
				int weight = Integer.parseInt(weightInput.getText());
				if (weight < MIN_WEIGHT ) {
					JOptionPane.showMessageDialog(null, "Too light: valid weight is [" + MIN_WEIGHT + "," +  MAX_WEIGHT + "]");
				} else if (weight > MAX_WEIGHT ) {
					JOptionPane.showMessageDialog(null, "Too heavy: valid weight is [" + MIN_WEIGHT + "," +  MAX_WEIGHT + "]");
				} else {
					super.handleOKButton();
				}
			}
		} catch (NumberFormatException ex) {
			JOptionPane.showMessageDialog(null, "Please input only numeric input for weight");
		}
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV11 mobileDeviceFormV11 = 
				new MobileDeviceFormV11("Mobile Device Form V11");
		mobileDeviceFormV11.addComponents();
		mobileDeviceFormV11.addMenus();
		mobileDeviceFormV11.setFrameFeatures();
		mobileDeviceFormV11.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
}
