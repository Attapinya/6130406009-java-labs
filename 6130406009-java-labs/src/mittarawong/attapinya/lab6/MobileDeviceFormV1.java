package mittarawong.attapinya.lab6;

import java.awt.*;
import javax.swing.*;

/**
 * This java program MobileDeviceFormV1 extends from MySimpleWindow 
 * and create two methods are setFrameFeatures and addComponents.
 * addComponents method create three panels for panelNorth, panelSouth and panel.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-03-03
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MobileDeviceFormV1 extends MySimpleWindow {

	public MobileDeviceFormV1(String string) {
		super(string);
	}

	protected JPanel panelNorth, panelSouth, panelAll, panelOS;
	protected JLabel labelBrand, labelModel, labelWeight, labelPrice, labelOS;
	protected JTextField brandInput, modelInput, weightInput, priceInput;
	protected JRadioButton android, ios;
	protected ButtonGroup osGrp;

	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	protected void addComponents() {
		super.addComponents();

		panelNorth = new JPanel();
		panelNorth.setLayout(new GridLayout(0, 2));
		
		labelBrand = new JLabel("Brand Name:");
		panelNorth.add(labelBrand);
		brandInput = new JTextField(15);
		panelNorth.add(brandInput);
		
		labelModel = new JLabel("Model Name:");
		panelNorth.add(labelModel);
		modelInput = new JTextField(15);
		panelNorth.add(modelInput);
		
		labelWeight = new JLabel("Weight (kg.):");
		panelNorth.add(labelWeight);
		weightInput = new JTextField(15);
		panelNorth.add(weightInput);
		
		labelPrice = new JLabel("price (Baht):");
		panelNorth.add(labelPrice);
		priceInput = new JTextField(15);
		panelNorth.add(priceInput);
		
		labelOS = new JLabel("Mobile OS:");
		panelNorth.add(labelOS);
		panelOS = new JPanel();
		android = new JRadioButton("Android");
		ios = new JRadioButton("iOS");
		osGrp = new ButtonGroup();
		osGrp.add(android);
		osGrp.add(ios);
		panelOS.add(android);
		panelOS.add(ios);
		panelNorth.add(panelOS);
		
		panelSouth = new JPanel();	
		panelSouth.add(panel);
		
		panelAll = new JPanel();
		panelAll.setLayout(new BorderLayout());
		panelAll.add(panelNorth, BorderLayout.NORTH);
		panelAll.add(panelSouth, BorderLayout.SOUTH);

		this.add(panelAll);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV1 mobileDeviceFormV1 = new MobileDeviceFormV1("Mobile Device Form V1");
		mobileDeviceFormV1.addComponents();
		mobileDeviceFormV1.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
