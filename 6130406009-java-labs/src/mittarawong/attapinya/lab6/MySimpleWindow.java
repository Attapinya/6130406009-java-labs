package mittarawong.attapinya.lab6;

import java.awt.*;
import javax.swing.*;

/**
 * This java program MySimpleWindow create two methods are setFrameFeatures and addComponents.
 * addComponents method create two buttons for cancel and ok.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-03-03
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MySimpleWindow extends JFrame {
	
	public MySimpleWindow(String string) {
		super(string);
	}
	
	protected JPanel panel;
	protected JButton cancelButton, okButton;
	
	private static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();	
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	protected void setFrameFeatures() {
		this.pack();
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	protected void addComponents() {
		cancelButton = new JButton("Cancel");
		okButton = new JButton("OK");
		
		panel = new JPanel();
		
		panel.add(cancelButton);
		panel.add(okButton);
		
		this.setLayout(new FlowLayout());
		
		this.add(panel);
	}
}
