package mittarawong.attapinya.lab6;

import java.awt.*;
import javax.swing.*;

/**
 * This java program MobileDeviceFormV2 extends from MobileDeviceFormV1
 * and create two methods are setFrameFeatures and addComponents.
 * addComponents method create eight panels for panel_north, panel_center,
 * panel_south, panel_type, panel_review, sub_type, sub_review and panel.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-03-03
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MobileDeviceFormV2 extends MobileDeviceFormV1 {

	public MobileDeviceFormV2(String string) {
		super(string);
	}

	protected JPanel panelCenter, panel_type, panel_review;
	protected JLabel labelType, labelReview;
	protected JTextArea textArea;
	protected JComboBox typeChoice;
	
	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	protected void addComponents() {
		super.addComponents();
		
		labelType = new JLabel("Type:");
		panelNorth.add(labelType);
		
		typeChoice = new JComboBox<String>();
		typeChoice.addItem("Phone");
		typeChoice.addItem("Tablet");
		typeChoice.addItem("Smart TV");
		panelNorth.add(typeChoice);
		
		panelCenter = new JPanel();
		panelCenter.setLayout(new BorderLayout());
		
		panel_review = new JPanel();
		panel_review.setLayout(new BorderLayout());
		labelReview = new JLabel("Review:");
		panel_review.add(labelReview, BorderLayout.NORTH);
		
		JPanel sub_review = new JPanel();
		textArea = new JTextArea(3, 35);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setText("Bigger than previous Note phones in every way, "
				+ "the Samsung Galaxy Note 9 has a larger 6.4-inch screen, "
				+ "heftier 4,000mAh battery, and a massive 1TB of storage option.");
		JScrollPane scrollpane = new JScrollPane(textArea);
		sub_review.add(scrollpane);
		panel_review.add(sub_review, BorderLayout.SOUTH);
		
		panelCenter.add(panel_review);
		
		panelAll.add(panelCenter, BorderLayout.CENTER);
		
		this.add(panelAll);
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV2 mobileDeviceFormV2 = 
				new MobileDeviceFormV2("Mobile Device Form V2");
		mobileDeviceFormV2.addComponents();
		mobileDeviceFormV2.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
}
	