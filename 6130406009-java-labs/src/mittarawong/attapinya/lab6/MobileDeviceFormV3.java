package mittarawong.attapinya.lab6;

import java.awt.*;
import javax.swing.*;

/**
 * This java program MobileDeviceFormV3 extends from MobileDeviceFormV2
 * and create two methods are setFrameFeatures and addComponents.
 * addComponents method create three panels for panel_features, sub_features
 * and panel.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-03-03
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MobileDeviceFormV3 extends MobileDeviceFormV2 {

	public MobileDeviceFormV3(String string) {
		super(string);
	}

	protected JPanel panel_features, panelCenter_V3;
	protected JMenuBar menubar;
	protected JMenuItem submenuNew, submenuOpen, submenuSave, submenuExit, submenuColor, submenuSize;
	protected JMenu menuFile, menuConfig;
	protected JLabel labelFeatures;
	protected JList jlist;
	
	protected void setFrameFeatures() {
		super.setFrameFeatures();
	}

	protected void addComponents() {
		super.addComponents();
		
		panel_features = new JPanel();
		panel_features.setLayout(new GridLayout(0, 2));
		labelFeatures = new JLabel("Features:");
		panel_features.add(labelFeatures);
		
		JPanel sub_features = new JPanel();
		String label[] = {"Design and build quality", "Great Camera", "Screen", "Battery Life" };
		jlist = new JList<String>(label);
		sub_features.add(jlist);
		panel_features.add(sub_features);
		
		panelCenter_V3 = new JPanel();
		panelCenter_V3.setLayout(new BorderLayout());
		panelCenter_V3.add(panel_features, BorderLayout.NORTH);
		panelCenter_V3.add(panelCenter, BorderLayout.SOUTH);
		
		panelAll.add(panelCenter_V3, BorderLayout.CENTER);

		this.add(panelAll);
	}
	
	protected void addMenus() {
		menubar = new JMenuBar();
		
		menuFile = new JMenu("File");
		submenuNew = new JMenuItem("New");
		menuFile.add(submenuNew);
		submenuOpen = new JMenuItem("Open");
		menuFile.add(submenuOpen);
		submenuSave = new JMenuItem("Save");
		menuFile.add(submenuSave);
		submenuExit = new JMenuItem("Exit");
		menuFile.add(submenuExit);
		menubar.add(menuFile);

		menuConfig = new JMenu("Config");
		submenuColor = new JMenuItem("Color");
		menuConfig.add(submenuColor);
		submenuSize = new JMenuItem("Size");
		menuConfig.add(submenuSize);
		menubar.add(menuConfig);

		this.setJMenuBar(menubar);
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV3 mobileDeviceFormV3 = 
				new MobileDeviceFormV3("Mobile Device Form V3");
		mobileDeviceFormV3.addComponents();
		mobileDeviceFormV3.addMenus();
		mobileDeviceFormV3.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
}
