package mittarawong.attapinya.lab8;

import javax.swing.SwingUtilities;

/**
 * This java program MyFrameV4 accept value from class MyCanvasV4 and run program shown in display.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-02
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyFrameV4 extends MyFrameV3 {

	public MyFrameV4(String text) {
		super(text);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				CreateAndShowGUI();
			}
		});
	}
	
	public static void CreateAndShowGUI() {
		MyFrameV4 msw = new MyFrameV4("My Frame V4");
		msw.addComponents();
		msw.setFrameFeatures();	
	}

	protected void addComponents() {
		add(new MyCanvasV4());
	}
}
