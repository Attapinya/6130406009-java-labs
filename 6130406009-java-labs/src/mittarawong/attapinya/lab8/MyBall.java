package mittarawong.attapinya.lab8;

import java.awt.geom.Ellipse2D;

/**
 * This java program MyBall has one variable and accepts two parameters
 * then call superclass constructor passing the input parameters,
 * and diameter as parameters.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-02
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyBall extends Ellipse2D.Double {
	
	public final static int diameter = 30;
	
	public MyBall(int x, int y) {
		super(x, y, diameter, diameter);
	}
}
