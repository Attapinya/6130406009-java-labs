package mittarawong.attapinya.lab8;

import java.awt.geom.Rectangle2D;

/**
 * This java program MyPedal has two variables and accepts two parameters
 * then call superclass constructor passing the input parameters,
 * and pedalWidth and pedalHeight as parameters.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-02
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyPedal extends Rectangle2D.Double {
	
	public final static int pedalWidth = 100;
	public final static int pedalHeight = 10;
	
	public MyPedal(int x, int y) {
		super(x, y, pedalWidth, pedalHeight);
	}
}
