package mittarawong.attapinya.lab8;

import java.awt.geom.Rectangle2D;

/**
 * This java program MyBrick has two variables and accepts two parameters
 * then call superclass constructor passing the input parameters,
 * and brickWidth and brickHeight as parameters.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-02
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyBrick extends Rectangle2D.Double {
	
	public final static int brickWidth = 80;
	public final static int brickHeight = 20;
	
	public MyBrick(int x, int y) {
		super(x, y, brickWidth, brickHeight);
	}
}
