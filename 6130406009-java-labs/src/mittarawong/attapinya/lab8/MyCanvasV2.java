package mittarawong.attapinya.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

/**
 * This java program has 3 variables are ball, pedal and brick.
 * and value settings for drawing according to the question
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-02
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyCanvasV2 extends MyCanvas {

	MyBall ball;
	MyPedal pedal;
	MyBrick brick;

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Rectangle2D rec = new Rectangle2D.Double(0, 0, 800, 600);
		ball = new MyBall(385, 285);
		pedal = new MyPedal(350, 590);
		brick = new MyBrick(360, 0);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.black);
		g2d.fill(rec);
		g2d.setColor(Color.white);
		g2d.fill(ball);
		g2d.fill(pedal);
		g2d.fill(brick);
		g2d.drawLine(0, 300, 800, 300);
		g2d.drawLine(400, 0, 400, 600);
	}
}
