package mittarawong.attapinya.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

/**
 * This java program has 2 variables are arbrick and color.
 * and value settings for drawing according to the question
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-02
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyCanvasV4 extends MyCanvasV3 {
	
	MyBrick arbrick[][] = new MyBrick[7][10];
	Color color[] = {Color.magenta, Color.blue, Color.cyan, Color.green, Color.yellow, Color.orange, Color.red};

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Rectangle2D rec = new Rectangle2D.Double(0, 0, 800, 600);
		ball = new MyBall((WIDTH - MyBall.diameter) / 2, HEIGHT - (MyBall.diameter + MyPedal.pedalHeight));
		pedal = new MyPedal((WIDTH - MyPedal.pedalWidth) / 2, HEIGHT - MyPedal.pedalHeight);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.black);
		g2d.fill(rec);
		
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 10; j++) {
				arbrick[i][j] = new MyBrick(j * MyBrick.brickWidth, (HEIGHT / 3) - (MyBrick.brickHeight * i));
				g2d.setColor(color[i]);
				g2d.fill(arbrick[i][j]);
				g2d.setColor(Color.black);
				g2d.setStroke(new BasicStroke(5));
				g2d.draw(arbrick[i][j]);
			}
		}
		
		g2d.setColor(Color.white);
		g2d.fill(ball);
		g2d.setColor(Color.gray);
		g2d.fill(pedal);
	}
}
