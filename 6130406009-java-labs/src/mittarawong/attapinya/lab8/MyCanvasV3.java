package mittarawong.attapinya.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;

/**
 * This java program has 2 variables are arball and arbrick.
 * and value settings for drawing according to the question
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-02
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyCanvasV3 extends MyCanvasV2 {
	
	MyBall arball[] = new MyBall[4];
	MyBrick arbrick[] = new MyBrick[10];
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Rectangle2D rec = new Rectangle2D.Double(0, 0, 800, 600);
		arball[0] = new MyBall(0, 0);
		arball[1] = new MyBall(WIDTH - MyBall.diameter, 0);
		arball[2] = new MyBall(0, HEIGHT - MyBall.diameter);
		arball[3] = new MyBall(WIDTH - MyBall.diameter, HEIGHT - MyBall.diameter);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.black);
		g2d.fill(rec);
		g2d.setColor(Color.white);
		for (int i = 0; i < 4; i++) {
			g2d.fill(arball[i]);
		}
		
		for (int j = 0; j < 10; j++) {
			arbrick[j] = new MyBrick(j * MyBrick.brickWidth, (HEIGHT - MyBrick.brickHeight) / 2);
			g2d.setColor(Color.white);
			g2d.fill(arbrick[j]);
			g2d.setColor(Color.black);
			g2d.setStroke(new BasicStroke(5));
			g2d.draw(arbrick[j]);
		}
	}
}
