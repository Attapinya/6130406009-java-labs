package mittarawong.attapinya.lab8;

import javax.swing.SwingUtilities;

/**
 * This java program MyFrameV3 accept value from class MyCanvasV3 and run program shown in display.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-02
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyFrameV3 extends MyFrameV2 {

	public MyFrameV3(String text) {
		super(text);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				CreateAndShowGUI();
			}
		});
	}
	
	public static void CreateAndShowGUI() {
		MyFrameV3 msw = new MyFrameV3("My Frame V3");
		msw.addComponents();
		msw.setFrameFeatures();	
	}

	protected void addComponents() {
		add(new MyCanvasV3());
	}
}
