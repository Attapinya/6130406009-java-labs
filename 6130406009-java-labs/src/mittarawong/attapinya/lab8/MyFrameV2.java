package mittarawong.attapinya.lab8;

import javax.swing.SwingUtilities;

/**
 * This java program MyFrameV2 accept value from class MyCanvasV2 and run program shown in display.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-02
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyFrameV2 extends MyFrame {

	public MyFrameV2(String text) {
		super(text);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				CreateAndShowGUI();
			}
		});
	}
	
	public static void CreateAndShowGUI() {
		MyFrameV2 msw = new MyFrameV2("My Frame V2");
		msw.addComponents();
		msw.setFrameFeatures();	
	}

	protected void addComponents() {
		add(new MyCanvasV2());
	}
}