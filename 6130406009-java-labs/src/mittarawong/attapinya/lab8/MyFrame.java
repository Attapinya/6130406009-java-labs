package mittarawong.attapinya.lab8;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * This java program MyFrame accept value from class MyCanvas and run program shown in display.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-02
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyFrame extends JFrame {

	public MyFrame(String text) {
		super(text);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable () {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public static void createAndShowGUI() {
		MyFrame msw = new MyFrame("My Frame");
		msw.addComponents();
		msw.setFrameFeatures();	
	}

	protected void addComponents() {
		add(new MyCanvas());
	}
	
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
