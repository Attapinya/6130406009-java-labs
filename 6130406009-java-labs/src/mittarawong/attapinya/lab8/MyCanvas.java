package mittarawong.attapinya.lab8;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

/**
 * This java program has 2 variables for storing the width and height.
 * and value settings for drawing according to the question.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-02
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MyCanvas extends JPanel {
	
	public final static int WIDTH = 800;
	public final static int HEIGHT = 600;
	
	public MyCanvas() {
		super();
		setPreferredSize();
	}

	private void setPreferredSize() {
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setBackground(Color.black);
		setVisible(true);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.white);
		g2d.drawOval(230, 150, 300, 300);
		g2d.fillOval(330, 250, 30, 60);
		g2d.fillOval(400, 250, 30, 60);
		g2d.fillRect(330, 370, 100, 10);
	}
}
