package mittarawong.attapinya.lab2;

public class ComputeMoney {

	public static void main(String[] args) {
		//check four numbers for bank notes
		if (args.length != 4) {
			System.err.println ("ComputeMoney <1000 Baht> <500 Baht> <100 Baht> <20 Baht>");
		}
		//calculate total amount of money
		else {
			double oneT = Double.parseDouble(args[0]);
			double fiveH = Double.parseDouble(args[1]);
			double oneH = Double.parseDouble(args[2]);
			double twenty = Double.parseDouble(args[3]);
			
			double TotalT = oneT*1000;
			double TotalFH = fiveH*500;
			double TotalOH = oneH*100;
			double TotalTw = twenty*20;
			double Total = TotalT + TotalFH +TotalOH + TotalTw;
			System.out.println("Total Money is " + Total);
		}

	}

}

/**
* This Java program that accepts four arguments: your the number of notes of 1,000 Baht, 500 Baht,
* 100 Baht and 20 Baht. check enter all four numbers for bank notes
* and calculate total amount of money.
* The output of the program is in the format
* total amount of money.
*
* Author: Attapinya Mittarawong
* ID: 613040600-9
* Sec: 1
* Date: January 21, 2019
*
**/
