package mittarawong.attapinya.lab2;

import java.util.Arrays;

public class SortNumbers {

	public static void main(String[] args) {
		if (args.length == 5) {
			double number1 = Double.parseDouble(args[0]);
			double number2 = Double.parseDouble(args[1]);
			double number3 = Double.parseDouble(args[2]);
			double number4 = Double.parseDouble(args[3]);
			double number5 = Double.parseDouble(args[4]);
		
			double [] array = {number1, number2, number3, number4, number5};
		
			Arrays.sort(array);
		
			System.out.println(array[0] + " " + array[1] + " " + array[2] + " " +array[3] + " " +array[4]);
		}
		else {
			System.err.println ("Number: <number1> <number2> <number3> <number4> <number5> ");
		}
	}

}

/**
* This Java program that accepts five arguments: number1, number2, ..., number5.
* read and store those numbers in an array, and then use Java subroutine to sort these numbers.
* The output of the program is in the format
* output show each value individually ascending order.
*
* Author: Attapinya Mittarawong
* ID: 613040600-9
* Sec: 1
* Date: January 21, 2019
*
**/
