package mittarawong.attapinya.lab2;

public class StringAPI {
	public static void main(String[] args) {
		if (args.length == 1) {
			String schoolName = (args[0]);
			boolean testUniversity = args[0].toLowerCase().contains("university");
			boolean testCollege = args[0].toLowerCase().contains("college");
			
			if (testUniversity == true) {
				System.out.println(schoolName + " is a university");
			}
			else if (testCollege == true) {
				System.out.println(schoolName + " is a college");
			}
			else {
				System.out.println(schoolName + " is neither a university nor a college");
			}
		}
		else {
			System.err.println("<your school name>");
		}

	}

}

/**
* This Java program that accepts one arguments: your school name.
* Save that argument in the variable called schoolName 
* and check whether the variable schoolName has the substring “college” or “university” or other.
* The output of the program is in the format
* output shown type when input your school name.
*
* Author: Attapinya Mittarawong
* ID: 613040600-9
* Sec: 1
* Date: January 21, 2019
*
**/
