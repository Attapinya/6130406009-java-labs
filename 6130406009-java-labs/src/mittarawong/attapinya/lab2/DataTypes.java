package mittarawong.attapinya.lab2;

public class DataTypes {
	public static void main(String[] args) {
		String name = "Attapinya Mittarawong";
		String id = "6130406009";
		char firstLetter = name.charAt(0);
		boolean value = true;
		int idOctal = 011;
		int hexadecimal = 0x9;
		long twoLong = 9l ;
		float twoFloat = 09.61f;
		double lastDouble = 09.61d;
		
		System.out.println ("My name is " + name);
		System.out.println ("My student ID is " + id);
		System.out.print (firstLetter + " ");
		System.out.print (value + " ");
		System.out.print (idOctal + " ");
		System.out.println (hexadecimal + " ");
		System.out.print (twoLong + " ");
		System.out.print (twoFloat + " ");
		System.out.print (lastDouble + " ");
		
	}

}

/**
* This Java program that keep arguments: name, student ID, value = true, first letter of name, 
* variable with an octal value, a hexadecimal value, a long value, a float variable with value 
* and a double variable with value
* The output of the program is in the format
* output all the variables in this order.
*
* Author: Attapinya Mittarawong
* ID: 613040600-9
* Sec: 1
* Date: January 21, 2019
*
**/
