package mittarawong.attapinya.lab4;

/**
 * This java program ToyotaAuto will receive the information of the Toyota brand.
 * And calculate the value stored in each method.
 * 
 * @author Attapinya Mittarawong
 * Sec : 1
 * ID : 613040600-9
 * Date : 07/02/2019
 *
 */

public class ToyotaAuto extends Automobile {
	
	/**
	 * 
	 * @param maximum input argument for MaxSpeed
	 * @param acceleration input argument for Acceleration
	 * @param model input argument for Model
	 */

	public ToyotaAuto (int maximum, int acceleration, String model) {
		setGasoline(100);
		setMaxSpeed(maximum);
		setAcceleration(acceleration);
		setModel(model);
	}
	
	public void refuel() {
		setGasoline(100);
		System.out.println(getModel() + " refuels");
	}

	public void accelerate() {
		int acceler = getAcceleration();
		int speed = getSpeed();
		int maxS = getMaxSpeed();
		int gasso = getGasoline();
		
		if ((speed + acceler) > maxS) {
			setSpeed(maxS);
			setGasoline(gasso - 15);
			System.out.println(getModel() + " accelerates");
		}
		else {
			setSpeed(speed + acceler);
			setGasoline(gasso - 15);
			System.out.println(getModel() + " accelerates");
		}
	}
	
	public void brake() {
		int acceler = getAcceleration();
		int speed = getSpeed();
		int gasso = getGasoline();
		
		if ((speed - acceler) > 0) {
			setSpeed(speed);
			setGasoline(gasso - 15);
			System.out.println(getModel() + " brakes");
		}
		else {
			setSpeed(0);
			setGasoline(gasso - 15);
			System.out.println(getModel() + " brakes");
		}
	}
	
	public void setSpeed() {
		int inputSpeed = getSpeed();
		int maxS = getMaxSpeed();
		
		if (inputSpeed < 0) {
			setSpeed(0);
		}
		else if (inputSpeed > maxS){
			setMaxSpeed(maxS);
		}
	}

	@Override
	public String toString() {
		return getModel() + " gas:" + getGasoline() + " speed:" + getSpeed() + " max speed:"
				+ getMaxSpeed() + " acceleration:" + getAcceleration();
	}
}
