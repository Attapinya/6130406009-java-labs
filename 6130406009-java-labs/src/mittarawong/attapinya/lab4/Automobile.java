package mittarawong.attapinya.lab4;

/**
 * This java program Automobile will receive the information of the car
 * in each model and calculate the value.
 * 
 * @author Attapinya Mittarawong
 * Sec : 1
 * ID : 613040600-9
 * Date : 07/02/2019
 *
 */

public class Automobile {
	
	private int gasoline;
	private int speed;
	private int maxSpeed;
	private int acceleration;
	private String model;
	private Color color;
	private static int numberOfAutomobile = 0;
	
	/**
	 * 
	 * @return The getGasoline method return the value of the variable gasoline.
	 */
	public int getGasoline() {
		return gasoline;
	}

	/**
	 * 
	 * @param gasoline : The setGasoline method takes a parameter (gasoline) and assigns it to the gasoline variable. 
	 * The this keyword is used to refer to the current object.
	 */
	public void setGasoline(int gasoline) {
		this.gasoline = gasoline;
	}

	/**
	 * 
	 * @return The getSpeed method return the value of the variable speed.
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * 
	 * @param speed : The setSpeed method takes a parameter (speed) and assigns it to the speed variable. 
	 * The this keyword is used to refer to the current object.
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	/**
	 * 
	 * @return The getMaxSpeed method return the value of the variable maxSpeed.
	 */
	public int getMaxSpeed() {
		return maxSpeed;
	}

	/**
	 * 
	 * @param maxSpeed : The setMaxSpeed method takes a parameter (maxSpeed) and assigns it to the maxSpeed variable. 
	 * The this keyword is used to refer to the current object.
	 */
	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	/**
	 * 
	 * @return The getAcceleration method return the value of the variable acceleration.
	 */
	public int getAcceleration() {
		return acceleration;
	}

	/**
	 * 
	 * @param acceleration : The setAcceleration method takes a parameter (acceleration) and assigns it to the acceleration variable. 
	 * The this keyword is used to refer to the current object.
	 */
	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}

	/**
	 * 
	 * @return The getModel method return the value of the variable model.
	 */
	public String getModel() {
		return model;
	}

	/**
	 * 
	 * @param model : The setModel method takes a parameter (model) and assigns it to the model variable. 
	 * The this keyword is used to refer to the current object.
	 */
	public void setModel(String model) {
		this.model = model;
	}

	/**
	 * 
	 * @return The getColor method return the value of the variable color.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * 
	 * @param color : The setColor method takes a parameter (color) and assigns it to the color variable. 
	 * The this keyword is used to refer to the current object.
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * 
	 * @return The getNumberOfAutomobile method return the value of the variable numberOfAutomobile.
	 */
	public static int getNumberOfAutomobile() {
		return numberOfAutomobile;
	}

	/**
	 * 
	 * @param numberOfAutomobile : The setNumberOfAutomobile method takes a parameter (numberOfAutomobile) 
	 * and assigns it to the numberOfAutomobile variable. 
	 * The this keyword is used to refer to the current object.
	 */
	public static void setNumberOfAutomobile(int numberOfAutomobile) {
		Automobile.numberOfAutomobile = numberOfAutomobile;
	}

	/**
	 * this method is return the value in a String.
	 */
	@Override
	public String toString() {
		return "Automobile [gasoline=" + gasoline + ", speed=" + speed + ", maxSpeed=" + maxSpeed + ", acceleration="
				+ acceleration + ", model=" + model + ", color=" + color + "]";
	}

	enum Color {
		RED,
		ORANGE,
		YELLOW,
		GREEN,
		BLUE,
		INDIGO,
		VIOLET,
		WHITE,
		BLACK
	}
	
	/**
	 * constructor program.
	 */
	public Automobile() {
		this.gasoline = 0;
		this.speed =0;
		this.maxSpeed = 160;
		this.acceleration = 0;
		this.model = "Automobile";
		this.color = Color.WHITE;
		numberOfAutomobile += 1;
	}
	
	/**
	 * constructor program and input value.
	 */
	public Automobile(int gasoline, int speed, int maxSpeed, int acceleration, String model, Color color) {
		this.gasoline = gasoline;
		this.speed = speed;
		this.maxSpeed = maxSpeed;
		this.acceleration = acceleration;
		this.model = model;
		this.color = color;
		numberOfAutomobile += 1;
	}
}
