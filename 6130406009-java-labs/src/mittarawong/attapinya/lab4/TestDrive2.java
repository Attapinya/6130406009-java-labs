package mittarawong.attapinya.lab4;

/**
 * This java program TestDrive will receive the data calculated from ToyotaAuto and HondaAuto.
 * And calculate the value stored in each method and compare the speed of both brands.
 * 
 * @author Attapinya Mittarawong
 * Sec : 1
 * ID : 613040600-9
 * Date : 07/02/2019
 *
 */

public class TestDrive2 {

	/**
	 * This is main method of the program.
	 * 
	 * @param args This argument receive user input.
	 */
	public static void main(String[] args) {
		ToyotaAuto car1 = new ToyotaAuto(200, 10, "Vios");
		HondaAuto car2 = new HondaAuto(220, 8, "City");
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.accelerate();
		car2.accelerate();
		car2.accelerate();
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.brake();
		car1.brake();
		car2.brake();
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.refuel();
		car2.refuel();
		System.out.println(car1);
		System.out.println(car2);
		isFaster(car1, car2);
	}

	/**
	 * This method is This method is used to compare the car's speed.
	 * 
	 * @param car1 is the Variable used to store the speed of the Toyota car.
	 * @param car2 is the Variable used to store the speed of the Honda car.
	 */
	public static void isFaster(Automobile car1, Automobile car2) {
		int speed1 = car1.getSpeed();
		int speed2 = car2.getSpeed();
		if (speed1 > speed2) {
			System.out.println(car2.getModel() + " is NOT faster than " + car1.getModel());
			System.out.println(car1.getModel() + " is faster than " + car2.getModel());
		}
		else {
			System.out.println(car1.getModel() + " is NOT faster than " + car2.getModel());
			System.out.println(car2.getModel() + " is faster than " + car1.getModel());
		}
		
	}
}
