package mittarawong.attapinya.lab4;

import mittarawong.attapinya.lab4.Automobile.Color;

/**
 * This java program TestDrive will receive the data calculated from Automobile
 * and show the result according to the calculated value.
 * 
 * @author Attapinya Mittarawong
 * Sec : 1
 * ID : 613040600-9
 * Date : 07/02/2019
 * 
 */

public class TestDrive {
	
	/**
	 * This is main method of the program.
	 * 
	 * @param args input argument.
	 */
	
	public static void main(String[] args) {
		Automobile car1 = new Automobile(100, 0, 200, 0, "Sport car", Color.RED);
		Automobile car2 = new Automobile(100, 0, 100, 0, "Electric car", Color.WHITE);
		System.out.println("Car 1's max speed is " + car1.getMaxSpeed());
		System.out.println("Car 2's max speed is " + car2.getMaxSpeed());
		car1.setMaxSpeed(280);
		System.out.println("Car 1's max speed has increased to " + car1.getMaxSpeed());
		System.out.println("There are " + Automobile.getNumberOfAutomobile() + " automobile");
		Automobile car3 = new Automobile();
		System.out.println("Now there are " + Automobile.getNumberOfAutomobile() + " automobile");
		System.out.println(car1);
		System.out.println(car3);
	}
}
