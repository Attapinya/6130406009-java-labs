package mittarawong.attapinya.lab4;

/**
 * This java program Interface Moveable.
 * 
 * @author Attapinya Mittarawong
 * Sec : 1
 * ID : 613040600-9
 * Date : 07/02/2019
 *
 */

public interface Movable {
	
	public void accelerate();
	
	public void brake();
	
	public void setSpeed();
}
