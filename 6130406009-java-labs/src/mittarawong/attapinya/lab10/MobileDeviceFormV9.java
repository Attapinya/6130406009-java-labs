package mittarawong.attapinya.lab10;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 * When user chooses the Open or Save menu item, open or save file chooser dialog is displayed.
 * When user chooses the menu items Red, Green, Blue or Custom, the program will change the text color as the user selects.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-22
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MobileDeviceFormV9 extends MobileDeviceFormV8 implements ActionListener {
	
	JFileChooser fileChooser;
	protected int returnval;

	public MobileDeviceFormV9(String topic) {
		super(topic);
	}
	
	protected void addListeners() {
		super.addListeners();
		
		submenu_open.addActionListener(this);
		submenu_save.addActionListener(this);
		submenu_exit.addActionListener(this);
		subcolor_red.addActionListener(this);
		subcolor_green.addActionListener(this);
		subcolor_blue.addActionListener(this);
		subcolor_custom.addActionListener(this);
		
	}

	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		Object src = event.getSource();
		fileChooser = new JFileChooser();

		if (src == submenu_open) {
			returnval = fileChooser.showOpenDialog(MobileDeviceFormV9.this);
			
			if (returnval == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				JOptionPane.showMessageDialog(null, "Opening file " + file.getName());
			}
			
			else {
				JOptionPane.showMessageDialog(null, "Open Command cancelled by user");
			}
		}
		
		else if (src == submenu_save) {
			returnval = fileChooser.showSaveDialog(MobileDeviceFormV9.this);
			
			if (returnval == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				JOptionPane.showMessageDialog(null, "Saving file " + file.getName());
			}
			
			else {
				JOptionPane.showMessageDialog(null, "Save Command cancelled by user");
			}
		}
		
		else if (src == submenu_exit) {
			System.exit(DO_NOTHING_ON_CLOSE);
		}
		
		else if (src == subcolor_red) {
			textArea.setBackground(Color.red);
		}
		
		else if (src == subcolor_green) {
			textArea.setBackground(Color.green);
		}
		
		else if (src == subcolor_blue) {
			textArea.setBackground(Color.blue);
		}
		
		else if (src == subcolor_custom) {
			Color color = JColorChooser.showDialog(MobileDeviceFormV9.this, "Choose Color", textArea.getBackground());
			textArea.setBackground(color);
		}
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV9 mobileDeviceFormV9 = 
				new MobileDeviceFormV9("Mobile Device Form V9");
		mobileDeviceFormV9.addComponents();
		mobileDeviceFormV9.addMenus();
		mobileDeviceFormV9.setFrameFeatures();
		mobileDeviceFormV9.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
}
