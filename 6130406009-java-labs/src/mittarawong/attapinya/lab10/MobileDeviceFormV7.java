package mittarawong.attapinya.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import mittarawong.attapinya.lab7.MobileDeviceFormV6;

/**
 * Gets the text entered or has been set for all components and then displays all of this information in a dialog
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-22
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MobileDeviceFormV7 extends MobileDeviceFormV6 implements ActionListener, ItemListener, ListSelectionListener {

	public MobileDeviceFormV7(String topic) {
		super(topic);
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		Object src = event.getSource();
		if (src == okButton) {
			handleOKButton();
		}
		
		else if (src == cancelButton) {
			handleCancelButton();
		}
	}
	
	@Override
	public void itemStateChanged(ItemEvent event) {
		if (event.getSource() == android || event.getSource() == ios) {
			if (android.isSelected()) {
				JOptionPane.showMessageDialog(null, "Your os platform is now changed to Android");
			}
			
			else if (ios.isSelected()) {
				JOptionPane.showMessageDialog(null, "Your os platform is now changed to iOS");
			}
		}
		
		else if (event.getStateChange() == ItemEvent.SELECTED) {
			JOptionPane.showMessageDialog(null, "Type is update to " + typeChoice.getSelectedItem());
		}
		
	}
	
	private void handleCancelButton() {
		brandInput.setText(null);
		modelInput.setText(null);
		weightInput.setText(null);
		priceInput.setText(null);
		textArea.setText(null);
	}

	protected void handleOKButton() {
		if (android.isSelected()) {
			JOptionPane.showMessageDialog(null, "Brand Name: " + brandInput.getText() + ", Model Name: " + modelInput.getText()
										+ ", Weight: " + weightInput.getText() + ", Price: " + priceInput.getText()
										+ "\nOS: " + android.getText() + "\nType: " + typeChoice.getSelectedItem()
										+ "\nFeatures: " + jlist.getSelectedValuesList() + "\nReview: " + textArea.getText());

		}
		
		else if (ios.isSelected()) {
			JOptionPane.showMessageDialog(null, "Brand Name: " + brandInput.getText() + ", Model Name: " + modelInput.getText()
										+ ", Weight: " + weightInput.getText() + ", Price: " + priceInput.getText()
										+ "\nOS: " + ios.getText() + "\nType: " + typeChoice.getSelectedItem()
										+ "\nFeatures: " + jlist.getSelectedValuesList() + "\nReview: " + textArea.getText());
		}
	}
	
	@Override
	public void valueChanged(ListSelectionEvent event) {
		if (jlist.getValueIsAdjusting()) {
			JOptionPane.showMessageDialog(null, jlist.getSelectedValuesList());
		}
	}
	
	protected void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		android.addItemListener(this);
		ios.addItemListener(this);
		typeChoice.addItemListener(this);
		jlist.addListSelectionListener(this);
	}
	
	protected void addComponents() {
		super.addComponents();
	
		jlist.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		android.setSelected(true);
		jlist.setSelectedIndices(new int[] {0,1,3});
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV7 mobileDeviceFormV7 = 
				new MobileDeviceFormV7("Mobile Device Form V7");
		mobileDeviceFormV7.addComponents();
		mobileDeviceFormV7.addMenus();
		mobileDeviceFormV7.setFrameFeatures();
		mobileDeviceFormV7.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
}
