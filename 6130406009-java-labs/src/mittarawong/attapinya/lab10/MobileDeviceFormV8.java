package mittarawong.attapinya.lab10;

import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

/**
 * Implement accelerators and mnemonic keys to menu File, Config and their menu items.
 * 
 * @author Attapinya Mittarawong
 * @since 2019-04-22
 * Sec : 1
 * ID : 613040600-9
 *
 */

public class MobileDeviceFormV8 extends MobileDeviceFormV7 {
	
	JMenuItem subcolor_custom;

	public MobileDeviceFormV8(String topic) {
		super(topic);
	}

	protected void addSubMenu() {
		super.addSubMenu();

		subcolor_custom = new JMenuItem("Custom ...");
		submenu_color.add(subcolor_custom);
		
		submenu_color.setMnemonic(KeyEvent.VK_L);
		
		subcolor_red.setMnemonic(KeyEvent.VK_R);
		subcolor_red.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.CTRL_MASK));
		
		subcolor_green.setMnemonic(KeyEvent.VK_G);
		subcolor_green.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, KeyEvent.CTRL_MASK));
		
		subcolor_blue.setMnemonic(KeyEvent.VK_B);
		subcolor_blue.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, KeyEvent.CTRL_MASK));
		
		subcolor_custom.setMnemonic(KeyEvent.VK_U);
		subcolor_custom.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, KeyEvent.CTRL_MASK));
		
	}
	
	protected void addMenus() {
		super.addMenus();
		
		menuFile.setMnemonic(KeyEvent.VK_F);
		
		submenu_new.setMnemonic(KeyEvent.VK_N);
		submenu_new.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_MASK));
		
		submenu_open.setMnemonic(KeyEvent.VK_O);
		submenu_open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_MASK));
		
		submenu_save.setMnemonic(KeyEvent.VK_S);
		submenu_save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_MASK));
		
		submenu_exit.setMnemonic(KeyEvent.VK_X);
		submenu_exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_MASK));
		
		menuConfig.setMnemonic(KeyEvent.VK_C);
		
	}
	
	public static void createAndShowGUI() {
		MobileDeviceFormV8 mobileDeviceFormV8 = 
				new MobileDeviceFormV8("Mobile Device Form V8");
		mobileDeviceFormV8.addComponents();
		mobileDeviceFormV8.addMenus();
		mobileDeviceFormV8.setFrameFeatures();
		mobileDeviceFormV8.addListeners();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});		
	}
}
