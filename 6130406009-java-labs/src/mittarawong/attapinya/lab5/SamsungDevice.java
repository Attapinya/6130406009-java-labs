package mittarawong.attapinya.lab5;

/**
 * 
 * SamsungDevice is object of MobileDevice by extends MobileDevice.
 * 
 * @author Attapinya Mittarawong
 * @version 1.0
 * @since 2019-02-19
 *
 */
public class SamsungDevice extends MobileDevice {
	
	private static String brand  = "Samsung";
	private double androidVersion;
	
	/**
	 * This is constructor accept 3 parameters.
	 * 
	 * @param modelName  		accept model name set to modelName in MobileDevice.
	 * @param price 			accept price set to price in MobileDevice.
	 * @param androidVersion 	accept android version set to androidVersion in SamsungDevice.
	 */
	public SamsungDevice(String modelName, int price, double androidVersion) {
		super(modelName, "Android", price);
		this.androidVersion = androidVersion;
	}

	/**
	 * This is constructor accept 4 parameters.
	 * 
	 * @param modelName			accept model name set to modelName in MobileDevice.
	 * @param price				accept price set to price in MobileDevice.
	 * @param weight			accept weight set to weight in MobileDevice.
	 * @param androidVersion	accept android version set to androidVersion in SamsungDevice.
	 */
	public SamsungDevice(String modelName, int price, int weight, double androidVersion) {
		super(modelName, "Android", price, weight);
		this.androidVersion = androidVersion;
	}
	
	public static String getBrand() {
		return brand;
	}

	public static void setBrand(String brand) {
		SamsungDevice.brand = brand;
	}

	public double getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(double androidVersion) {
		this.androidVersion = androidVersion;
	}

	@Override
	public String toString() {
		return  "SumsungDevice[" + super.output() + ", Android version:" + getAndroidVersion() + "]";
	}

	public void displayTime() {
		System.out.println("Display times in both using a digital format and using an analog watch");
	}
	
}
