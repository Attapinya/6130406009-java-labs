package mittarawong.attapinya.lab5;

/**
 * 
 * AndroidDevice is abstract class that have abstract method name is usage.
 * 
 * @author Attapinya Mittarawong
 * @version 1.0
 * @since 2019-02-19
 *
 */
public abstract class AndroidDevice {
	
	abstract void usage();
	private static String os = "Android";
	public String getOs() {
		return os;
	}
}
