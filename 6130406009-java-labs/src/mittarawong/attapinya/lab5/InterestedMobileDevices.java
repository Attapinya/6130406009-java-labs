package mittarawong.attapinya.lab5;

/**
 * 
 * @author Attapinya Mittarawong
 * @version 1.0
 * @since 2019-02-19
 *
 */
public class InterestedMobileDevices {

	public static void main(String[] args) {
		MobileDevice galaxyNote9 = new MobileDevice("Galaxy Note 9", "Android", 25500, 201);
		MobileDevice iPadGen6 = new MobileDevice("Apple iPad Mini 3", "ios", 11500);
		System.out.println(galaxyNote9);
		System.out.println(iPadGen6);
		iPadGen6.setPrice(11000);
		System.out.println(iPadGen6.getModelName() + " has new price as " + iPadGen6.getPrice() + " Baht.");
		System.out.println(galaxyNote9.getModelName() + " has " + "weight as " + galaxyNote9.getWeight() + " grams.");
	}

}
