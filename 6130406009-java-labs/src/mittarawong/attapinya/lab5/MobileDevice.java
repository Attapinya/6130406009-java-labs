package mittarawong.attapinya.lab5;

/**
 * 
 * MobileDevice is super class.
 * And is to model a mobile device object with attributes modelName, os, price, and weight.
 * 
 * @author Attapinya Mittarawong
 * @version 1.0
 * @since 2019-02-19
 *
 */
public class MobileDevice {
	
	private String modelName;
	private String os;
	private int price;
	private int weight;

	/**
	 * This is constructor accept 4 parameters.
	 * 
	 * @param modelNumber	accept model number set to modelNumber in MobileDevice.
	 * @param os			accept os set to os in MobileDevice.
	 * @param price			accept price set to price in MobileDevice.
	 * @param weight		accept weight set to weight in MobileDevice.
	 */
	public MobileDevice(String modelNumber, String os, int price, int weight) {
		this.modelName = modelNumber;
		this.os = os;
		this.price = price;
		this.weight = weight;
	}

	/**
	 * This is constructor accept 3 parameters.
	 * 
	 * @param modelNumber	accept model number set to modelNumber in MobileDevice.
	 * @param os			accept os set to os in MobileDevice.
	 * @param price			accept price set to price in MobileDevice.
	 */
	public MobileDevice(String modelNumber, String os, int price) {
		this.modelName = modelNumber;
		this.os = os;
		this.price = price;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	/**
	 * 
	 * @return The output method return the value.
	 */
	public String output() {
		return "Model Name:" + modelName + ", OS: " + os + ", Price:" + price + " Baht, Weight:" + weight + " g";
	}
	
	@Override
	public String toString() {
		return "MobileDevice [" + output() + "]";
	}

}
