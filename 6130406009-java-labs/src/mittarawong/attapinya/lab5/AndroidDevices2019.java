package mittarawong.attapinya.lab5;

/**
 * 
 * @author Attapinya Mittarawong
 * @version 1.0
 * @since 2019-02-19
 *
 */
public class AndroidDevices2019 {

	public static void main(String[] args) {
		AndroidDevice ticwatchPro = new AndroidSmartWatch("Mobvoi", "TicWatch Pro", 8390);
		AndroidSmartWatch xiaomiMiBand3 = new AndroidSmartWatch("Xiaomi", "Mi Band 3", 950);
		ticwatchPro.usage();
		System.out.println(ticwatchPro);
		System.out.println(xiaomiMiBand3);
	}

}
