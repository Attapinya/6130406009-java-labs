package mittarawong.attapinya.lab5;

/**
 * 
 * AndroidSmartWatch is object of AndroidDevice by extends AndroidDevice.
 * 
 * @author Attapinya Mittarawong
 * @version 1.0
 * @since 2019-02-19
 *
 */
public class AndroidSmartWatch extends AndroidDevice {
	
	private String modelName, brandName;
	private int price;

	/**
	 * This is constructor accept 3 parameters.
	 * 
	 * @param brandName	accept brand name set to brandName in AndroidDevice.
	 * @param modelName	accept model name set to modelName in AndroidDevice.
	 * @param price		accept price set to price in AndroidDevice.
	 */
	public AndroidSmartWatch(String brandName, String modelName, int price) {
		this.modelName = modelName;
		this.brandName = brandName;
		this.price = price;
	}

	@Override
	void usage() {
		System.out.println("AndroidSmartWatch Usage: Show time, date, your heart rate, and your step count");
		
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "AndroidSmartWatch [Brand name:" + brandName + ", Model name:" + modelName + ", price:" + price + " Baht]";
	}

	public void displayTime() {
		System.out.println("Display time only using a digital format");
	}
	
}
