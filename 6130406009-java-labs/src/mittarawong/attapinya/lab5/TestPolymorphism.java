package mittarawong.attapinya.lab5;

/**
 * 
 * @author Attapinya Mittarawong
 * @version 1.0
 * @since 2019-02-19
 *
 */
public class TestPolymorphism {

	public static void main(String[] args) {
		AndroidSmartWatch ticwathchPro = 
				new AndroidSmartWatch("Mobvoi", "TicWatch Pro", 8390);
		SamsungDevice note9 = new SamsungDevice("Galaxy Note 9", 25500, 201, 
				8.1);
		System.out.println(ticwathchPro);
		ticwathchPro.displayTime();
		System.out.println();
		System.out.println(note9);
		note9.displayTime();
	}

}




